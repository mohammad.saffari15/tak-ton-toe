import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var PlayerOneLabel: UILabel!
    @IBOutlet weak var PlayerTwoLabel: UILabel!
    internal var playerone = 0
    internal var playerTwo = 0
    @IBOutlet weak var btn0: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //Buttons Action
    
    internal var isfirst:Bool = true
    internal var PositionCells:[[Int]] = Array (repeating : Array(repeating :0 , count: 3),count :3)
    @IBAction func BtnTuchup(_ sender: UIButton) {
    let row = Int(sender.tag / 3)
    let column = (sender.tag % 3)
        if PositionCells[row][column] != 0 {
          return
        }
        if isfirst {
            PositionCells[row][column] = 1
            let ImageO = UIImage(named :"o")
            sender.setImage(ImageO, for: UIControlState.normal)
        }else {
            PositionCells[row][column] = 2
            let Imagex = UIImage(named :"X")
            sender.setImage(Imagex, for: UIControlState.normal)
        }
        
    isfirst = !isfirst
        
        for i in 0...2 {
            if PositionCells[i][0] != 0 && PositionCells[i][0] == PositionCells[i][1] && PositionCells[i][1] == PositionCells[i][2] {
                CheckWinner(Playernumber: PositionCells[i][0])
            }
        }
        for j in 0...2 {
            if PositionCells[0][j] != 0 && PositionCells[0][j] == PositionCells[1][j] && PositionCells[1][j] == PositionCells[2][j] {
                CheckWinner(Playernumber: PositionCells[0][j])
            }
        }
        if PositionCells[0][0] != 0 && PositionCells[0][0] == PositionCells[1][1] && PositionCells[1][1] == PositionCells[2][2] {
            CheckWinner(Playernumber: PositionCells[0][0])
    }
        if  PositionCells[0][2] != 0 && PositionCells[0][2] == PositionCells[1][1] && PositionCells[1][1] == PositionCells[2][0] {
            CheckWinner(Playernumber: PositionCells[0][2])
        }
        var test = 9
        for i in 0...2 {
            for j in 0...2 {
                if PositionCells[i][j] != 0 {
                    test -= 1
                }
            }
        }
        if test == 0{
            let alert = UIAlertController(title: "Finish", message: "Game Is Equal", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Main Menu", style: UIAlertActionStyle.default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
                
            }))
            alert.addAction(UIAlertAction(title: "Restart", style: UIAlertActionStyle.default, handler: { (action) in
                self.Restartgame()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    
    }
    internal func Alertshow(Playernumber : Int) {
        let alert = UIAlertController(title: "Finish", message: "Playe \(Playernumber) are WIN", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Main Menu", style: UIAlertActionStyle.default, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Restart", style: UIAlertActionStyle.default, handler: { (action) in
            self.Restartgame()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    internal func Restartgame() {
        for m in 0...8 {
            if let btn = view.viewWithTag(m) as? UIButton {
                btn.setImage(nil, for: UIControlState.normal)
            }}
        PositionCells = Array (repeating : Array(repeating :0 , count: 3),count :3)
        btn0.setImage(nil, for: UIControlState.normal)
        
    }
    internal func  CheckWinner (Playernumber : Int) {
       
        if Playernumber == 1 {
            playerone += 1
            PlayerOneLabel.text! += String(playerone)
        }else {
            playerTwo += 1
            PlayerTwoLabel.text! += String(playerTwo)
        }
        Alertshow(Playernumber: Playernumber)
    }
    
    
    
}
